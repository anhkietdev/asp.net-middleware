﻿namespace MyApplication.Middlewares
{
    public class MyMiddleware : IMiddleware
    {
        
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            await context.Response.WriteAsync("\nMiddleware 2 - Start");
            await next(context);
            await context.Response.WriteAsync("\nMiddleware 2 - End");
        }
    }

    public static class MyMiddlewareExtension
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<MyMiddleware>();
        }
    }
}
