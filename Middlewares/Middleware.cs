﻿namespace MyApplication.Middlewares
{
    /// <summary>
    /// Đây là class built-in nhé :). Tạo rồi sửa cho phù hợp
    /// </summary>
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class Middleware
    {
        private readonly RequestDelegate _next;

        public Middleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            //http://localhost:5189/?firstname=kiet&lastname=truong
            if (httpContext.Request.Query.ContainsKey("firstname") && 
                httpContext.Request.Query.ContainsKey("lastname"))
            {
                string fullName = httpContext.Request.Query["firstname"] + " " +
                                  httpContext.Request.Query["lastname"];

                await httpContext.Response.WriteAsync(fullName);
            }
            await _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<Middleware>();
        }
    }
}
