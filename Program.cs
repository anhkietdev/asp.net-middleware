using MyApplication.Middlewares;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddTransient<MyMiddleware>();
var app = builder.Build();

//to do: example of using UseWhen and MapWhen.

app.Use(async (HttpContext context, RequestDelegate next) =>
{
    //Middleware 1
    await context.Response.WriteAsync("Middleware 1 - Start");
    await next(context);
    await context.Response.WriteAsync("\nMiddleware 1 - End");

});

//Old way using customized middleware
//app.UseMiddleware<MyMiddleware>();

//New way using extension method
app.UseMyMiddleware();

//New new new way using built-int middleware format
app.UseMiddleware();


app.Run(async (HttpContext context) =>
{
    await context.Response.WriteAsync("\nMiddleware 3 (terminal)");
});

app.Run();
